package com.everis.demo.test_framework.steps;

import com.everis.demo.test_framework.utils.BrowserDriver;
import com.everis.demo.test_framework.utils.DataCredentials;
import com.everis.demo.test_framework.utils.FunctionalUtils.ActionsUtils;
import com.everis.demo.test_framework.utils.FunctionalUtils.BrowserUtils;
import com.everis.demo.test_framework.utils.FunctionalUtils.WaitUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class SCALoginSteps {

    @Autowired
    private ActionsUtils actionsUtils;
    @Autowired
    private WaitUtils waitUtils;
    @Autowired
    private BrowserDriver browserDriver;
    @Autowired
    private BrowserUtils browserUtils;

    @Given("^I access to SCA home page$")
    public void goToSimulationPage() throws InterruptedException {
        browserUtils.navigateTo(DataCredentials.Pages.NEW_SIMULATION_PAGE_HOME);
    }


}


