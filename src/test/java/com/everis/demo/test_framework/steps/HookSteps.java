package com.everis.demo.test_framework.steps;

import com.everis.demo.test_framework.utils.*;
import com.everis.demo.test_framework.utils.FunctionalUtils.ActionsUtils;
import com.everis.demo.test_framework.utils.FunctionalUtils.WaitUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.runtime.ScenarioImpl;
import gherkin.formatter.model.Result;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.openqa.selenium.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@PropertySource("classpath:app.properties")
public class HookSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(HookSteps.class);
    @Value("${video.initialization:yes}")
    private String video;
    @Autowired
    private BrowserDriver browserDriver;

    @Autowired
    private ActionsUtils actionsUtils;

    @Autowired
    private WaitUtils waitUtils;

    private static Recorder recorder;


    @Before
    public void openBrowser(Scenario scenario) {
        LOGGER.info("SCENARIO: " + scenario.getName());

        try {
            browserDriver.getCurrentDriver();
            browserDriver.getCurrentDriver().manage().window().maximize();
        } catch (RuntimeException ignore) {
            ignore.getCause();
        }
        if(video.equals("yes")){
            try {
                recorder = ScreenRecorderFactory.makeRecorder("./target/videos", scenario.getName(), false);
                recorder.start();
            } catch(IOException | AWTException e) {
                LOGGER.error("Error al crear/iniciar el ScreenRecorder: " + e.getMessage());
            }
        }

    }

    @After
    public void embedScreenshot(Scenario scenario) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
        }
        if(video.equals("yes")){
        try {
            recorder.stop();
            List<File> createdMovieFiles = recorder.getCreatedMovieFiles();
            for(File movie : createdMovieFiles) {
                LOGGER.debug("Nuevo video creado: " + movie.getAbsolutePath());
            }
        } catch(IOException e) {
            LOGGER.error("Error parando el ScreenRecorder: " + e.getMessage());
        }}

        try {
            actionsUtils.takeScreenshot(scenario);
            System.out.println("Screenshot made");
        } catch (Exception ignore) {
            ignore.getCause();
        }

        if(scenario.isFailed()){
            logError(scenario);
            try{
                Alert alert = waitUtils.waitUntilAlert(10);
                LOGGER.debug("Alert no controlado: "+ alert.getText());
                alert.accept();
            }catch(Exception ignore){
                LOGGER.error(ignore.getMessage());
            }
        }

        browserDriver.closeBrowser();

        LOGGER.info("END SCENARIO: " + scenario.getName());
        LOGGER.info("RESULTADO: " + (scenario.isFailed()?"error":"correcto") + "\n");
    }

    private static void logError(Scenario scenario) {
        Field field = FieldUtils.getField(((ScenarioImpl) scenario).getClass(), "stepResults", true);
        field.setAccessible(true);
        try {
            ArrayList<Result> results = (ArrayList<Result>) field.get(scenario);
            for (Result result : results) {
                if (result.getError() != null){
                    LOGGER.error("ERROR SCENARIO: {}", scenario.getId(), result.getError());
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error while logging error", e);
        }
    }

}
