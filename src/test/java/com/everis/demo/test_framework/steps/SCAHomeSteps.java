package com.everis.demo.test_framework.steps;

import com.everis.demo.test_framework.pageobjects.SCAHome.SCAHomeService;
import com.everis.demo.test_framework.utils.BrowserDriver;
import com.everis.demo.test_framework.utils.FunctionalUtils.ActionsUtils;
import com.everis.demo.test_framework.utils.FunctionalUtils.BrowserUtils;
import com.everis.demo.test_framework.utils.FunctionalUtils.WaitUtils;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class SCAHomeSteps {

     @Autowired
    private ActionsUtils actionsUtils;
    @Autowired
    private WaitUtils waitUtils;
    @Autowired
    private BrowserDriver browserDriver;
    @Autowired
    private BrowserUtils browserUtils;
    @Autowired
    private SCAHomeService scaHomeService;

    @When("^I click on medical insurance$")
    public void iClickOnMedicalInsurance() {
        scaHomeService.clickMedicalInsurance();

    }

}
