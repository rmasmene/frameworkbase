package com.everis.demo.test_framework;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = "src/test/resources/cucumber_features/",
        plugin = {"json:target/cucumber-report/cucumber.json","pretty"},
        monochrome = false,
        tags = "~@ignore"
)
public class TestRunner {
}
