package com.everis.demo.test_framework.utils.Validations;

public class AssertMessages {
    public static class Messages {
        public static final String SEARCHSIMULATIONERROR = "No se ha encontrado la simulación buscada";
        public static final String SIMULATIONNOTDELETED = "La simulación no se ha eliminado";
        public static final String NOPRINTPAGE = "No ha llegado a la ventana \"Imprimir\"";
        public static final String NOREQUESTPAGE = "No ha llegado a la ventana de Alta De Solicitud";
        public static final String NOPRINCIPALINREQUEST = "No se ha cargado el titular de la simulación en la solicitud";
        public static final String NORESULTS = "No se han cargado resultados";
        public static final String NOCHECKMYSIM = "El check 'Mis simulaciones' no está checkeado";
        public static final String NOFILEEXPORTED = "No se ha encontrado el fichero exportado";
        public static final String TARIFERROR = "La simulación tiene un error de tarificación";
        public static final String MAXSUPPLTRY = "Las pólizas utilizadas han dado errores de datos.";
        public static final String ADDMODULEERROR = "El asegurado no dispone de módulos para contratar";
        public static final String EGASERROR = "Error EGAS";
        public static final String INDEXERROR = "Error, no se ha pasado un índice válido al método";
        public static final String SIMULATIONDELETED ="La eliminación de la simulación";
        public static final String ERRORDELETE ="no se ha borrado correctamente";
        public static final String NO_SECOND_SCREEN = "No se abre una segunda pantalla";
        public static final String NO_RESULTS = "no hay resultados válidos";
        public static final String ERROR_PRINT = "no se ha cargado la impresión correctamente ";
        public static final String CERTERROR = "Error, no aparece la pantalla de certificado";
        public static final String AUTHERROR = "Error de autenticación del usuario";
        public static final String DELEGATIONERROR = "Error, no carga el dropdown delegaciones";
        public static final String CITYERROR = "Error, no cargan los datos del dropdown de localidad";
        public static final String PREFIXERROR = "Error, no carga el dropdown de prefijos";
        public static final String PROVINCEERROR = "Error, no carga el dropdown de provincias";
        public static final String MODERROR = "Error, no se ha modificado correctamente";
        public static final String MEDIATORPREFILLED = "El dato precargado en la modificación del mediador no coincide";
        public static final String BILLSERROR ="La página de recibos no carga";
        public static final String ACCIDENTSERROR ="La página de accidentes no carga";
        public static final String MOVEMENTSERROR ="La página de movimientos no carga";
        public static final String SENDTYPEERROR ="No se ha guardado el cambio de tipo de envío";
    }
}
