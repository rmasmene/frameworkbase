package com.everis.demo.test_framework.utils.FunctionalUtils;

import com.everis.demo.test_framework.utils.BrowserDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JSUtils {
    @Autowired
    private BrowserDriver browserDriver;

    public void clickJS(WebElement element){
        JavascriptExecutor executor = (JavascriptExecutor)browserDriver.getCurrentDriver();
        executor.executeScript("arguments[0].click();", element);
    }

    public void scrollToElement(WebElement element){
        JavascriptExecutor js = ((JavascriptExecutor) browserDriver.getCurrentDriver());
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public Object executeJavascriptCommand(String command) {
        return ((JavascriptExecutor) browserDriver.getCurrentDriver()).executeScript(command);
    }
}
