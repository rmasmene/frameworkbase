package com.everis.demo.test_framework.utils.FunctionalUtils;

public class ElementsUtils {


    public static class LinksAndTabs {
        public static final String CHANGE_MEDIATOR = "Modificar Mediador";
        public static final String CHECK_DOCUMENTATION = "Consulta de documentación";
        public static final String ADDRESS = "Consultar/Modificar domicilio de envío del contrato";
        public static final String SEND_TYPE = "Consultar/Modificar tipo de envío";

        public static final String SUMMARY = "Resumen";
        public static final String BILLS = "Recibos";
        public static final String ACCIDENT = "Siniestros";
        public static final String MOVEMENTS = "Movimientos";
    }

    public static class Literals {
        public static final String SEGUR_CAIXA = "SegurCaixa";
        public static final String SEGUR_CAIXA_MASCOTAS = "SegurCaixa Mascotas";
        public static final String LEGAL_PROTECTION = "SegurCaixa Protección Jurídica";
        public static final String LEGAL_PROTECTION_INDIVIDUAL = "Jurídica Particulares";
        public static final String LEGAL_PROTECTION_SMO = "Jurídica PYMES";
        public static final String LEGAL_PROTECTION_FREELANCER = "Jurídica Autónomos";
        public static final String ELECTRICAL_APPLIANCE = "SegurCaixa Electrodomésticos";
        public static final String TRAVEL_ASSISTANCE = "SegurCaixa Asistencia en Viaje";
        public static final String NO_RESULTS = "no hay resultados válidos";
        public static final String TEXT_IN_PRINT = "El presente documento tiene carácter exclusivamente informativo acerca de las principales características del producto, de tal modo que no implica la cobertura alguna por parte de la aseguradora, ni de aceptación del riesgo por la misma. La información precontractual y contractual completa relativa al seguro se facilita en otros documentos.";
        public static final String TEXT_DOCUMENT = "Listado de documentos encontrados:";
        public static final String ACTIONS = "Acciones";
        public static final String TRAVEL_ASSISTANCE_FROM_TABLE = "SegurCaixa Asistencia En Viaje";
        public static final String LEGAL_PROTECTION_FROM_TABLE = "SegurCaixa Protección Jurídica Particulares";
        public static final String CNAE = "6202";
        public static final String TITLE_RECOVERY_SIMULATIONS = "Relación de simulaciones de micro";
        public static final String DISCOUNT20 = "20% Descuento empleado";
        public static final String DISCOUNT_FIRST_YEAR = "Descuento comercial 1er año";
        public static final String SCHEDULE_CONFIRMATION = "Actividad programada correctamente";
        public static final String PERRO = "Perro";
        public static final String GATO = "Gato";
        public static final String MODIFY = "Modificar";
        public static final String MEDIATOR = "Mediador";
    }

    public static class Chars {
        public static final String PREFIX = "+011";
        public static final String TWO = "2";
        public static final String MEDIATOR1 = "7661677";
        public static final String MEDIATOR2 = "7661678";
    }

    public static class LEGALSIMULATIONS {
        public static final String PARTICULAR = "Particulares";
        public static final String SMO = "Pymes";
        public static final String FREELANCER = "Autónomos";
    }

    public static class Strings {
        public static final String SEGUR_CAIXA = "SegurCaixa";
        public static final String SEGUR_CAIXA_MASCOTAS = "SegurCaixa Mascotas";
        public static final String LEGAL_PROTECTION = "SegurCaixa Protección Jurídica";
        public static final String LEGAL_PROTECTION_INDIVIDUAL = "Jurídica Particulares";
        public static final String LEGAL_PROTECTION_SMO = "Jurídica PYMES";
        public static final String LEGAL_PROTECTION_FREELANCER = "Jurídica Autónomos";
        public static final String ELECTRICAL_APPLIANCE = "SegurCaixa Electrodomésticos";
        public static final String TRAVEL_ASSISTANCE = "SegurCaixa Asistencia en Viaje";
        public static final String NO_RESULTS = "no hay resultados válidos";
        public static final String TEXT_IN_PRINT = "El presente documento tiene carácter exclusivamente informativo acerca de las principales características del producto, de tal modo que no implica la cobertura alguna por parte de la aseguradora, ni de aceptación del riesgo por la misma. La información precontractual y contractual completa relativa al seguro se facilita en otros documentos.";
        public static final String TEXT_DOCUMENT = "Listado de documentos encontrados:";
        public static final String ACTIONS = "Acciones";
        public static final String TRAVEL_ASSISTANCE_FROM_TABLE = "SegurCaixa Asistencia En Viaje";
        public static final String LEGAL_PROTECTION_FROM_TABLE = "SegurCaixa Protección Jurídica Particulares";
        public static final String CNAE = "6202";
        public static final String TITLE_RECOVERY_SIMULATIONS = "Relación de simulaciones de micro";
        public static final String DISCOUNT20 = "20% Descuento empleado";
        public static final String DISCOUNT_FIRST_YEAR = "Descuento comercial 1er año";
        public static final String SCHEDULE_CONFIRMATION = "Actividad programada correctamente";
        public static final String PERRO = "Perro";
        public static final String GATO = "Gato";
        public static final String MODIFY = "Modificar";
        public static final String MEDIATOR = "Mediador";
        public static final String USER = "USUARIO3TEST";
        public static final String URL_MEDICAL_INSURANCE  = "https://www.segurcaixaadeslas.es/es/seguros-medicos";


    }
}
