package com.everis.demo.test_framework.utils.FunctionalUtils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.KeyEvent;

public class DropdownUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(DropdownUtils.class);


    public static boolean setSelect(WebElement element, int index) throws InterruptedException, AWTException {
        boolean flag = false;
        int i = 0;
        LOGGER.debug("Indice para el select='" + index + "'");
        Select s = new Select(element);
        do {
            Thread.sleep(1000);
            try {
                s.selectByIndex(index);
                LOGGER.info(s.getFirstSelectedOption().getText());
                flag = true;
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                Robot r = new Robot();
                r.keyPress(KeyEvent.VK_TAB);
                r.keyRelease(KeyEvent.VK_TAB);
                i++;
                if (i == 40) {
                    break;
                }
            }
        } while (!flag);
        return flag;
    }


    public static boolean setSelect(WebElement element, String value) throws InterruptedException, AWTException {
        boolean flag = false;
        int i = 0;
        LOGGER.debug("Valor para el select='" + value + "'");
        Select s = new Select(element);
        do {
            Thread.sleep(1000);
            try {
                s.selectByValue(value);
                LOGGER.info(s.getFirstSelectedOption().getText());
                flag = true;
            } catch (Exception e) {
                LOGGER.info(e.getMessage());
                Robot r = new Robot();
                r.keyPress(KeyEvent.VK_TAB);
                r.keyRelease(KeyEvent.VK_TAB);
                i++;
                if (i == 40) {
                    break;
                }
            }
        } while (!flag);
        return flag;
    }
}
