package com.everis.demo.test_framework.utils;

public class DataCredentials {
    public static class Selenium {
        public static final String SELENIUM_HUB = "http://localhost:4444/wd/hub";
    }

    public static class Pages {
        public static final String NEW_SIMULATION_PAGE_HOME = "https://www.segurcaixaadeslas.es/es/particulares";
    }

}
