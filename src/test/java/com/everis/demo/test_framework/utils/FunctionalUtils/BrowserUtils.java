package com.everis.demo.test_framework.utils.FunctionalUtils;

import com.everis.demo.test_framework.utils.BrowserDriver;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class BrowserUtils {

    @Autowired
    private BrowserDriver browserDriver;

    @Autowired
    private JSUtils jsUtils;

    public void navigateTo(String url) {
        System.out.println("Navigating to "+url);
        jsUtils.executeJavascriptCommand("this.document.location = " + "\'" + url + "'");
    }

    public void deleteLocalStorage() {
        jsUtils.executeJavascriptCommand("window.localStorage.clear();");
    }

    public void loadPage(String url) {
        browserDriver.getCurrentDriver().get(url);
    }

    public Set<Cookie> getCookies() {
        return browserDriver.getCurrentDriver().manage().getCookies();
    }

    public void deleteCookies() {
        browserDriver.getCurrentDriver().manage().deleteAllCookies();
    }

    public String getItemFromLocalStorage(String item) {
        return (String) jsUtils.executeJavascriptCommand("return window.localStorage.getItem(" + "\'" + item + "');");
    }

    public void switchFrameByElement(WebElement webElement) {
        browserDriver.getCurrentDriver().switchTo().frame(webElement);
    }

    public void switchFrameByIndex(int count) {
        browserDriver.getCurrentDriver().switchTo().frame(count);
    }

    public void switchToDefaultContent() {
        browserDriver.getCurrentDriver().switchTo().defaultContent();
    }
}
