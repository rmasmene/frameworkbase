package com.everis.demo.test_framework.utils.FunctionalUtils;


import com.everis.demo.test_framework.utils.BrowserDriver;
import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import static com.everis.demo.test_framework.utils.FunctionalUtils.CommonUtils.convertSetToList;

@Service
public class ActionsUtils {

    private BrowserDriver browserDriver;
    private static final Logger LOGGER = LoggerFactory.getLogger(ActionsUtils.class);

    @Autowired
    public ActionsUtils(BrowserDriver browserDriver) {
        this.browserDriver = browserDriver;
    }

    public void browserConfiguration() {
        browserDriver.getCurrentDriver().manage().window().maximize();
    }

    public List<String> getHandleWindow() {
        return convertSetToList(browserDriver.getCurrentDriver().getWindowHandles());
    }

    public void switchWindow(List<String> windowList, int windowId) {
        browserDriver.getCurrentDriver().switchTo().window(windowList.get(windowId));
    }

    public void actionClick(WebElement element){
        Actions actions = new Actions(browserDriver.getCurrentDriver());
        actions.click(element).perform();
    }

    public void changeWindowAndClose(){
        browserDriver.getCurrentDriver().close();
        switchWindow(getHandleWindow(), 0);
    }

    public void accessToMainWindow(){
        int numVentanas = browserDriver.getCurrentDriver().getWindowHandles().size();
        LOGGER.debug("Numero de ventanas " +numVentanas);
        Set<String> windows = browserDriver.getCurrentDriver().getWindowHandles();
        browserDriver.getCurrentDriver().switchTo().window(windows.iterator().next());
    }

    public void takeScreenshot(Scenario scenario) throws IOException {
        File scrFile = ((TakesScreenshot) browserDriver.getCurrentDriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/target/report/" + scenario.getName() + ".png"));
        attachScreenshotReport(scenario, scrFile);
    }

    private static void attachScreenshotReport(Scenario scenario, File scrFile) throws IOException {
        byte[] data = FileUtils.readFileToByteArray(scrFile);
        scenario.embed(data, "image/png");
    }
}
