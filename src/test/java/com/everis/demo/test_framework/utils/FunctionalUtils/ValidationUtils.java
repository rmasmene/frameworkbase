package com.everis.demo.test_framework.utils.FunctionalUtils;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ValidationUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationUtils.class);

    public static void showErrors(List<String> list) {
        String errors = "";
        if (list.size() > 0) {
            for (String s : list) {
                errors = errors + s + "\n";
            }
            Assert.fail(errors);
        }
    }

    public static List<String> elementContains(List<String> list, WebElement element, String text) {
        LOGGER.debug("Se esta comparando " + element.getText() + " con " + text);
        if (!element.getText().contains(text)) {
            list.add("El elemento " + element.getText() + " no contiene el texto " + text);
        }
        return list;
    }

    public static void elementContains(WebElement element, String text){
        LOGGER.debug("¿Contiene "+ element.getText() + " el texto: "+text+ "?");
        if(!element.getText().contains(text)){
            LOGGER.error(element.getText() + " no contiene " + text);
            Assert.fail(element.getText() + " no contiene " + text);
        }else{
            LOGGER.debug(element.getText() + " contiene " + text);
        }
    }
}
