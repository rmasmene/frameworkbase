package com.everis.demo.test_framework.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CommonUtils {

    public static List<String> convertSetToList(Set<String> setToConvert) {
        List<String> mainList = new ArrayList<>();
        mainList.addAll(setToConvert);
        return mainList;
    }

    public void writeResults(String cif, String fileName) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            File file = new File(System.getProperty("user.dir") + File.separator + "DataFiles" + File.separator + fileName + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(cif);
            bw.newLine();
            System.out.println("información agregada!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                //Cierra instancias de FileWriter y BufferedWriter
                if (bw != null) bw.close();
                if (fw != null) fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }




}
