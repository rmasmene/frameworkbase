package com.everis.demo.test_framework.utils;

public class DataFiles {
    public static final String ARTIFICIAL_FILE = "ArtificialFile";
    public static final String SEPARATOR = "/";
    public static final String CLIENT_FILE = "ClientFile";
    public static final String POLICY_FILE = "Policy";
}
