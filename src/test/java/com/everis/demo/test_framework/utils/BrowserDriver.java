package com.everis.demo.test_framework.utils;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:app.properties")
public class BrowserDriver {
    private static final Logger LOGGER = LoggerFactory.getLogger(BrowserDriver.class);


    @Value("${driver.browser:internet explorer}")
    private String browser;

    @Value("${driver.path:${user.dir}/drivers/}")
    private String driversPath;
    
    @Value("${driver.initialization:auto}")
    private String initialization;

    @Value("${driver.auto.ie.version:}")
    private String autoInternetExplorerVersion;

    @Value("${driver.auto.chrome.version:}")
    private String autoChromeVersion;

    @Value("${driver.auto.firefox.version:}")
    private String autoFirefoxVersion;

    @Value("${driver.auto.architecture:}")
    private String autoArchitecture;


    private WebDriver driver;


    public WebDriver getCurrentDriver() {
        if (driver == null) {
            initDriver();
        }
        return driver;
    }

    private void initDriver() {
        LOGGER.debug("Navegador='" + browser + "'");
        LOGGER.debug("Inicializacion driver='" + initialization + "'");
        switch (browser.toUpperCase()) {
            case "CHROME":
                if("MANUAL".equalsIgnoreCase(initialization)) {
                    System.setProperty("webdriver.chrome.driver", driversPath + "chromedriver.exe");
                } else {
                    configAutoChrome();
                }
                driver = new ChromeDriver(getChromeOptions());
                break;
            case "INTERNET EXPLORER":
                if("MANUAL".equalsIgnoreCase(initialization)) {
                    System.setProperty("webdriver.ie.driver", driversPath + "IEDriverServer.exe");
                } else {
                    configAutoInternetExplorer();
                }
                driver = new InternetExplorerDriver(getInternetExplorerOptions());
                break;
            case "FIREFOX":
                if("MANUAL".equalsIgnoreCase(initialization)) {
                    System.setProperty("webdriver.gecko.driver", driversPath + "geckodriver.exe");
                } else {
                    configAutoFirefox();
                }
                driver = new FirefoxDriver(getFirefoxOptions());
                break;
        }
    }


    private ChromeOptions getChromeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments(
                "--disable-web-security",
                "--ignore-certificate-errors",
                "--allow-running-insecure-content",
                "--allow-insecure-localhost",
                "--no-sandbox"
        );

        return chromeOptions;
    }

    private InternetExplorerOptions getInternetExplorerOptions() {
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();

        capabilities.setCapability("nativeEvents", false);
        capabilities.setCapability("unexpectedAlertBehaviour", "accept");
        capabilities.setCapability("ignoreProtectedModeSettings", true);
        capabilities.setCapability("disable-popup-blocking", true);
        capabilities.setCapability("enablePersistentHover", true);
        capabilities.setCapability("ignoreZoomSetting", true);
        capabilities.setCapability("ie.ensureCleanSession", true);
        capabilities.setCapability("trustAllSSLCerficates", true);

        return new InternetExplorerOptions(capabilities);
    }

    private FirefoxOptions getFirefoxOptions() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();

        return firefoxOptions;
    }

    private void configAutoInternetExplorer() {
        LOGGER.debug("Version Internet Explorer='" + autoInternetExplorerVersion + "'");
        LOGGER.debug("Arquitectura='" + autoArchitecture + "'");
        WebDriverManager webDriverManager = WebDriverManager.iedriver();
        configArchitecture(webDriverManager);
        if(null != autoInternetExplorerVersion) {
            webDriverManager.version(autoInternetExplorerVersion);
        }
        webDriverManager.setup();
    }

    private void configAutoChrome() {
        LOGGER.debug("Version Chrome='" + autoChromeVersion + "'");
        LOGGER.debug("Arquitectura='" + autoArchitecture + "'");
        WebDriverManager webDriverManager = WebDriverManager.chromedriver();
        configArchitecture(webDriverManager);
        if(null != autoChromeVersion) {
            webDriverManager.version(autoChromeVersion);
        }
        webDriverManager.setup();
    }

    private void configAutoFirefox() {
        LOGGER.debug("Version Firefox='" + autoFirefoxVersion + "'");
        LOGGER.debug("Arquitectura='" + autoArchitecture + "'");
        WebDriverManager webDriverManager = WebDriverManager.firefoxdriver();
        configArchitecture(webDriverManager);
        if(null != autoFirefoxVersion) {
            webDriverManager.version(autoFirefoxVersion);
        }
        webDriverManager.setup();
    }

    private void configArchitecture(WebDriverManager webDriverManager) {
        if("32".equals(autoArchitecture)) {
            webDriverManager = webDriverManager.arch32();
        } else if("64".equals(autoArchitecture)) {
            webDriverManager = webDriverManager.arch64();
        }
    }

    public void closeBrowser() {
        if(driver != null) {
            try {
                driver.quit();
                driver = null;
            } catch (Exception ignore) { }
        }
    }
}
