package com.everis.demo.test_framework.utils.FunctionalUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CommonUtils {

    public static List<String> convertSetToList(Set<String> setToConvert) {
        List<String> mainList = new ArrayList<>();
        mainList.addAll(setToConvert);
        return mainList;
    }
}
