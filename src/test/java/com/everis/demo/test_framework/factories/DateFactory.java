package com.everis.demo.test_framework.factories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.apache.commons.lang3.RandomUtils.nextInt;

public class DateFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateFactory.class);

    public static String getTodayDate() {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String dateString = dateFormat.format(date);
        LOGGER.debug("TodayDate=" + dateString + "'");
        return dateString;
    }

    public static String actualDatePlusMonths(int i) {
        LocalDate startDate = LocalDate.now();
        if (i < 0) {
            startDate = startDate.minusMonths(Math.abs(i));

        } else if (i > 0) {
            startDate = startDate.plusMonths(i);
        }
        String date = startDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        LOGGER.debug("fecha='" + date + "'");
        return date;
    }

    public static String actualDatePlusDays(int i) {
        LocalDate startDate = LocalDate.now();
        if (i < 0) {
            startDate = startDate.minusDays(Math.abs(i));

        } else if (i > 0) {
            startDate = startDate.plusDays(i);
        }
        String date = startDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        LOGGER.debug("fecha='" + date + "'");
        return date;
    }

    public static String actualDatePlusYears(int i) {
        LocalDate startDate = LocalDate.now();
        if (i < 0) {
            startDate = startDate.minusDays(Math.abs(i));

        } else if (i > 0) {
            startDate = startDate.plusYears(i);
        }
        String date = startDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        LOGGER.debug("fecha='" + date + "'");
        return date;
    }

    public static String getRandomHourStartSchedule() {
        String schedule = Integer.toString(nextInt(10, 13));
        LOGGER.debug("schedule='" + schedule + "'");
        return schedule;
    }

    public static String getRandomHourEndSchedule() {
        String schedule = Integer.toString(nextInt(13, 17));
        LOGGER.debug("schedule='" + schedule + "'");
        return schedule;
    }

    public static String getRandomMinutesStartSchedule() {
        String schedule = Integer.toString(nextInt(10, 31));
        LOGGER.debug("schedule='" + schedule + "'");
        return schedule;
    }

    public static String getRandomMinutesEndSchedule() {
        String schedule = Integer.toString(nextInt(40, 60));
        LOGGER.debug("schedule='" + schedule + "'");
        return schedule;
    }
}
