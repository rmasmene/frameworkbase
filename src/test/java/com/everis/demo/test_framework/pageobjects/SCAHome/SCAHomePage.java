package com.everis.demo.test_framework.pageobjects.SCAHome;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SCAHomePage {

    @FindBy(id="subTabPartMedico")
    private WebElement MedicalInsuranceTag;

    public WebElement getMedicalInsuranceTag() {
        return MedicalInsuranceTag;
    }
}
