package com.everis.demo.test_framework.pageobjects.SCAHome;


import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.everis.demo.test_framework.utils.BrowserDriver;
import com.everis.demo.test_framework.utils.FunctionalUtils.WaitUtils;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.everis.demo.test_framework.utils.FunctionalUtils.ElementsUtils.Strings.URL_MEDICAL_INSURANCE;
import static org.junit.Assert.assertEquals;


@Service
public class SCAHomeServiceImpl implements SCAHomeService{

    private BrowserDriver browserDriver;
    private WaitUtils waitUtils;
    private SCAHomePage scaHomePage;

    private static final Logger LOGGER = LoggerFactory.getLogger(SCAHomeServiceImpl.class);

    @Autowired
    public SCAHomeServiceImpl(BrowserDriver browserDriver, WaitUtils waitUtils) {
        this.browserDriver = browserDriver;
        this.waitUtils = waitUtils;
    }

    @Override
    public void clickMedicalInsurance(){
        initPageFactory();
        medicalInsurance();
        checkMedicalInsurancesite();
    }


    private void initPageFactory(){
        scaHomePage = PageFactory.initElements(this.browserDriver.getCurrentDriver(), SCAHomePage.class);
    }

    private void medicalInsurance(){
        scaHomePage.getMedicalInsuranceTag().click();
        LOGGER.info("Se hace clic sobre tag: " +scaHomePage.getMedicalInsuranceTag().getText());
    }

    private void checkMedicalInsurancesite(){
        assertEquals("Página no encontrada",URL_MEDICAL_INSURANCE,browserDriver.getCurrentDriver().getCurrentUrl());
    }



}
